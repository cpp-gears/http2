#include "chttp.h"
#include <sys/socket.h>
#include <poll.h>

using namespace lib;

namespace impl {
	static const uint32_t httpEOH{ *(const uint32_t*)"\r\n\r\n" };
	static const inline std::regex httpHeaderArgsSplit("([^;=]+)(?:=([^;]+))?");
	static const inline std::regex httpUrlSplit(R"(^(?:(.+://)(([^:/\s]*)(?:\:(\d+))?))?(([^?\s]*)(?:\?(\S*))?)?)");

	static inline ssize_t httpReadRequest(chttp::fd_t fd, uint8_t*& buffer, size_t& nlength, std::string_view& method, std::string_view& uri, std::string_view& proto);
	static inline ssize_t httpReadResponse(chttp::fd_t fd, uint8_t*& buffer, size_t& nlength, std::string_view& proto, std::string_view& code, std::string_view& msg);
	static inline ssize_t httpReadLine(chttp::fd_t fd, uint8_t*& buffer, size_t& nlength, std::string_view& line);
	static inline ssize_t httpReadData(chttp::fd_t fd, uint8_t* buffer, size_t nlength);
	static inline std::string_view trim(std::string_view str);
	static inline std::string_view trim_lowercase(std::string_view str);
}

chttp::payload_t chttp::httpMakeResponse(ssize_t httpCode, std::string_view httpMsg, const std::unordered_map<std::string, std::string>& httpHeaders, const void* payloadData, size_t payloadLen) {
	std::stringstream reply;
	reply << "HTTP/1.1 " << (httpCode > 0 ? httpCode : 500) << " " << httpCodeMessage(httpCode, httpMsg) << "\r\n";

	if (payloadData && payloadLen) { reply << "Content-Length: " << payloadLen << "\r\n"; }
	
	for (auto&& [h, v] : httpHeaders) { reply << h << ": " << v << "\r\n"; }
	reply << "\r\n";

	if (payloadData && payloadLen) { reply.write((const char*)payloadData, payloadLen); }

	auto nwrite{ reply.tellp() };
	if (std::shared_ptr<uint8_t[]> buffer{ new uint8_t[nwrite] }; buffer) {
		reply.read((char*)buffer.get(), nwrite);
		return { buffer,nwrite };
	}
	return { nullptr,0 };
}

ssize_t chttp::httpReadRequest(chttp::fd_t fd, http::method_t& method, http::uri_t& uri, http::headers_t& headers, http::payload_t& payload, std::shared_ptr<uint8_t[]>& request, size_t max_request_size) {
	size_t nLength{ MaxHeaderSize };
	request = std::shared_ptr<uint8_t[]>(new uint8_t[nLength]);
	std::string_view sUri, sProto, sLine;
	uint8_t* buffer{ request.get() };
	if (auto res = impl::httpReadRequest(fd, buffer, nLength, method, sUri, sProto); res == 0) {
		uri.assign(sUri);
		headers.clear();

		for (; (res = impl::httpReadLine(fd, buffer, nLength, sLine)) == 0;) {
			if (auto npos{ sLine.find(':') }; npos != std::string_view::npos) {
				headers.emplace(impl::trim_lowercase({ sLine.data(), npos }), impl::trim({ sLine.data() + npos + 1, sLine.size() - (npos + 1) }));
			}
			else if (*((uint32_t*)(buffer - 4)) == impl::httpEOH) {
				break;
			}
			else {
				return -EBADMSG;
			}
		}

		if (auto sContentLength{ headers.number("content-length") }; sContentLength) {
			if (sContentLength < nLength) {
				payload = { std::shared_ptr<uint8_t[]>{request, buffer }, sContentLength };
			}
			else {
				payload = { std::shared_ptr<uint8_t[]>{new uint8_t[sContentLength]}, sContentLength };
			}
			if (res = impl::httpReadData(fd, payload.first.get(), payload.second); res != 0) {
				return res;
			}
		}
		else {
			payload.first.reset();
			payload.second = 0;
		}
	}
	else {
		return res;
	}
	return 0;
}

ssize_t chttp::httpReadResponse(chttp::fd_t fd, http::method_t& method, std::string_view& code, std::string_view& msg, http::headers_t& headers, http::payload_t& payload, std::shared_ptr<uint8_t[]>& response, size_t max_response_size) {
	size_t nLength{ MaxHeaderSize };
	response = std::shared_ptr<uint8_t[]>(new uint8_t[nLength]);
	std::string_view sUri, sProto, sLine;
	uint8_t* buffer{ response.get() };
	if (auto res = impl::httpReadResponse(fd, buffer, nLength, method, code, msg); res == 0) {
		headers.clear();
		for (; (res = impl::httpReadLine(fd, buffer, nLength, sLine)) == 0;) {
			if (auto npos{ sLine.find(':') }; npos != std::string_view::npos) {
				headers.emplace(impl::trim({ sLine.data(), npos }), impl::trim({ sLine.data() + npos + 1, sLine.size() - (npos + 1) }));
			}
			else if (*((uint32_t*)(buffer - 4)) == impl::httpEOH) {
				break;
			}
			else {
				return -EBADMSG;
			}
		}

		if (auto sContentLength{ headers.number("Content-Length") }; sContentLength) {
			if (sContentLength < nLength) {
				payload = { std::shared_ptr<uint8_t[]>{ response,response.get() + nLength }, sContentLength };
			}
			else {
				payload = { std::shared_ptr<uint8_t[]>{new uint8_t[sContentLength]}, sContentLength };
			}
			if (res = impl::httpReadData(fd, payload.first.get(), payload.second); res != 0) {
				return res;
			}
		}
		else {
			payload.first.reset();
			payload.second = 0;
		}
	}
	else {
		return res;
	}
	return 0;
}


ssize_t chttp::httpReadRequest(chttp::fd_t fd, size_t max_request_size) {
	http::method_t method; http::uri_t uri; http::headers_t headers; 
	http::payload_t payload; 
	std::shared_ptr<uint8_t[]> request;
	auto res = httpReadRequest(fd, method, uri, headers, payload, request, max_request_size);
	return res == 0 ? httpOnRequest(fd, method, uri, headers, payload, request) : httpOnError(fd, res);
}

ssize_t chttp::httpReadResponse(chttp::fd_t fd, size_t max_response_size) {
	http::method_t method; http::headers_t headers; std::string_view code, msg; http::payload_t payload; std::shared_ptr<uint8_t[]> response;
	auto res = httpReadResponse(fd, method, code, msg, headers, payload, response, max_response_size);
	if (res == 0) { return httpOnResponse(fd, method, code, msg, headers, payload, response); }
	return httpOnError(fd, res);
}

std::string_view chttp::httpCodeMessage(ssize_t code, std::string_view msg) {
	static std::string_view unsupported_code{ "Unknown HTTP error" };
	static const std::unordered_map<ssize_t, std::string_view> response_codes{
#include "chttpmsg.h"
	};
	if (msg.empty()) {
		if (code > 0) {
			if (auto&& it = response_codes.find(code); it != response_codes.end()) {
				return it->second;
			}
		}
		else {
			return response_codes.find(500)->second;
		}

		return unsupported_code;
	}
	return msg;
}

chttp::chttp() {
}

chttp::~chttp() {
}


std::string_view impl::trim(std::string_view str) {
	while (!str.empty() && std::isspace(str.front())) { str.remove_prefix(1); }
	while (!str.empty() && std::isspace(str.back())) { str.remove_suffix(1); }
	return str;
}

std::string_view impl::trim_lowercase(std::string_view str) {
	while (!str.empty() && std::isspace(str.front())) { str.remove_prefix(1); }
	while (!str.empty() && std::isspace(str.back())) { str.remove_suffix(1); }
	for (char* it{ (char*)str.data() }; it != str.end(); ++it) { *it = (char)std::tolower(*it); }
	return str;
}

ssize_t impl::httpReadResponse(chttp::fd_t fd, uint8_t*& data, size_t& nlength, std::string_view& proto, std::string_view& code, std::string_view& msg) {
	proto = { nullptr,0 };	code = { nullptr,0 }; msg = { nullptr,0 };
	std::string_view sLine;
	if (auto res = httpReadLine(fd, data, nlength, sLine); res == 0) {
		size_t npos{ 0 };
		if (npos = sLine.find(' '); npos != std::string_view::npos) {
			proto = { sLine.data(), npos };
			sLine.remove_prefix(npos + 1);
		}
		else {
			return -EBADMSG;
		}
		if (npos = sLine.find(' '); npos != std::string_view::npos) {
			code = { sLine.data(), npos };
			sLine.remove_prefix(npos + 1);
		}
		else {
			return -EBADMSG;
		}
		msg = impl::trim(sLine);
	}
	else {
		return res;
	}
	return 0;
}

ssize_t impl::httpReadRequest(chttp::fd_t fd, uint8_t*& data, size_t& nlength, std::string_view& method, std::string_view& uri, std::string_view& proto) {
	method = { nullptr,0 };	uri = { nullptr,0 }; proto = { nullptr,0 };
	std::string_view sLine;
	if (auto res = httpReadLine(fd, data, nlength, sLine); res == 0) {
		size_t npos{ 0 };
		if (npos = sLine.find(' '); npos != std::string_view::npos) {
			method = { sLine.data(), npos };
			sLine.remove_prefix(npos + 1);
		}
		else {
			return -EBADMSG;
		}
		if (npos = sLine.find(' '); npos != std::string_view::npos) {
			uri = { sLine.data(), npos };
			sLine.remove_prefix(npos + 1);
		}
		else {
			return -EBADMSG;
		}
		proto = impl::trim(sLine);
	}
	else {
		return res;
	}
	return 0;
}

ssize_t impl::httpReadLine(chttp::fd_t fd, uint8_t*& data, size_t& nlength, std::string_view& line) {
	ssize_t nread{ 0 }, nreaded{ 0 }, res{ 0 };
	uint8_t* buffer{ data };
	pollfd fds{ .fd = (int)fd.fd(),.events = POLLIN,.revents = 0 };
	do {
		for (; nlength && (res = fd.read(buffer, 1, nread, MSG_WAITALL | MSG_NOSIGNAL)) == 0 && nread == 1; ++buffer) {
			nlength--; nreaded++;
			if (*buffer == '\n') {
				line = { (char*)data, (size_t)nreaded };
				data = buffer + 1;
				return 0;
			}
		}
		fds.revents = 0;
	} while (res == -EAGAIN && (poll(&fds, 1, 100) > 0));
	return !nlength ? -EMSGSIZE : (!nread ? -ECONNABORTED : -errno);
}

ssize_t impl::httpReadData(chttp::fd_t fd, uint8_t* buffer, size_t nlength) {
	ssize_t nread{ 0 }, res{ -1 };
	pollfd fds{ .fd = (int)fd.fd(),.events = POLLIN,.revents = 0 };
	do {
		while (nlength && (res = fd.read(buffer, nlength, nread, MSG_NOSIGNAL)) == 0) {
			buffer += nread; nlength -= nread;
			continue;
		}
		if (nlength == 0) { return 0; }
		fds.revents = 0;
	} while (res == -EAGAIN && (poll(&fds, 1, 100) > 0));
	return res;
}

bool chttp::curi::cpath::match(size_t arg, const std::regex& re, std::vector<std::string_view>& result) const {
	if (arg < size()) {
		if (auto subject{ at(arg) }; !subject.empty()) {
			try {
				std::cmatch match;
				if (std::regex_match(subject.begin(), subject.end(), match, re) && match.size() > 1) {
					result.reserve(match.size());
					for (size_t n = 0; n < match.size(); ++n) {
						result.emplace_back(match[n].first, match[n].length());
					}
					return true;
				}
			}
			catch (std::regex_error& e) {
			}
		}
	}
	return false;
}

bool chttp::curi::cpath::match(size_t arg, const std::string_view& needle) const {
	if (arg < size()) {
		if (auto subject{ at(arg) }; !subject.empty()) {
			return subject.compare(needle) == 0;
		}
	}
	return false;
}

bool chttp::curi::cpath::match(size_t arg, const std::regex& re, std::string_view& result) const {
	if (arg < size()) {
		if (auto subject{ at(arg) }; !subject.empty()) {
			try {
				std::cmatch match;
				if (std::regex_match(subject.begin(), subject.end(), match, re) && match.size()) {
					result = std::string_view{ match[0].first, (size_t)match[0].length() };
					return true;
				}
			}
			catch (std::regex_error& e) {
			}
		}
	}
	return false;
}

std::unordered_map<std::string_view, std::string_view> chttp::cheaders::params(const std::string_view& header) const {
	std::unordered_map<std::string_view, std::string_view> params;
	if (auto&& it{ find(header) }; it != end()) {
		try {
			auto subject{ impl::trim(it->second) };
			std::cregex_iterator next(subject.begin(), subject.end(), impl::httpHeaderArgsSplit);
			std::cregex_iterator end;
			while (next != end) {
				std::cmatch match = *next;
				params.emplace(std::string_view{ match[0].first, (size_t)match[0].length() }, std::string_view{ match[1].first, (size_t)match[1].length() });
				next++;
			}
		}
		catch (std::regex_error& e) {
		}
	}
	return params;
}

std::unordered_set<std::string_view> chttp::cheaders::explode(const std::string_view& header, const char separator) const {
	std::unordered_set<std::string_view> args;
	if (auto&& it{ find(header) }; it != end()) {
		std::size_t start = 0;
		if (auto subject{ impl::trim(it->second) }; !subject.empty()) {
			std::size_t end = subject.find(separator);
			while (end != std::string_view::npos)
			{
				args.emplace(impl::trim({ subject.data() + start, static_cast<size_t>((subject.data() + end) - (subject.data() + start)) }));
				start = end + 1;
				end = subject.find(separator, start);
			}
			if (auto&& last = impl::trim({ subject.data() + start, subject.length() - start }); !last.empty()) {
				args.emplace(last);
			}
		}
	}
	return args;
}

bool chttp::cheaders::match(const std::string_view& header, const std::regex& re, std::vector<std::string_view>& result) const {
	result.clear();
	if (auto&& it{ find(header) }; it != end()) {
		if (auto subject{ impl::trim(it->second) }; !subject.empty()) {
			try {
				std::cmatch match;
				if (std::regex_search(subject.begin(), subject.end(), match, re) && match.size() > 1) {
					result.reserve(match.size() - 1);
					for (size_t n = 1; n < match.size(); ++n) {
						result.emplace_back(match[n].first, match[n].length());
					}
					return true;
				}
			}
			catch (std::regex_error& e) {
				// Syntax error in the regular expression
			}
		}
	}
	return false;
}

void chttp::curi::assign(std::string_view src) {
	try {
		std::cmatch match;
		if (std::regex_search(src.begin(), src.end(), match, impl::httpUrlSplit) && match.size() > 1) {
			full = src;
			schema = std::string_view{ match[1].first, (size_t)match[1].length() };
			hostport = std::string_view{ match[2].first, (size_t)match[2].length() };
			host = std::string_view{ match[3].first, (size_t)match[3].length() };
			port = std::string_view{ match[4].first, (size_t)match[4].length() };
			url = std::string_view{ match[5].first, (size_t)match[5].length() };
			path = std::string_view{ match[6].first, (size_t)match[6].length() };
			args = std::string_view{ match[7].first, (size_t)match[7].length() };

			if (!path.empty()) {
				std::string_view _path{ path };
				if (_path.front() == '/' || _path.front() == '*') { pathlist.emplace(_path.substr(0, 1)); _path.remove_prefix(1); }
				for (auto npos = _path.find_first_of('/'); npos != std::string_view::npos; _path.remove_prefix(npos + 1), npos = _path.find_first_of('/')) {
					pathlist.emplace(_path.substr(0, npos));
				}
				if (!_path.empty()) { pathlist.emplace(_path); }
			}

			if (!args.empty()) {
				std::string_view _args{ args }, name;
				do {
					if (auto npos = _args.find_first_of("=&"); npos != std::string_view::npos) {
						if (_args[npos] == '=') {
							name = _args.substr(0, npos);
							_args.remove_prefix(npos + 1);
						}
						else {
							argslist.emplace(name, _args.substr(0, npos));
							_args.remove_prefix(npos + 1);
							name = {};
						}
					}
					else {
						if (name.empty()) {
							argslist.emplace(_args, {""});
						}
						else {
							argslist.emplace(name, _args);
						}
						break;
					}
				} while (1);
			}
		}
	}
	catch (std::regex_error& e) {
		// Syntax error in the regular expression
	}
}