#include "chttp.h"
#include <sys/socket.h>
#include <poll.h>

using namespace lib;

ssize_t chttp::cfd::write_nossl(const void* data, size_t length, ssize_t& nwrited, uint flags) const {
	ssize_t nwrite{ 0 };
	uint8_t* buffer{ (uint8_t*)data };
	nwrited = 0;
	while (length && (nwrite = ::send((int)fdSocket, buffer, length, flags)) > 0) {
		length -= nwrite; nwrited += nwrite; buffer += nwrite;
	}
	return nwrite > 0 && !length ? 0 : -errno;
}

ssize_t chttp::cfd::write_ssl(const void* data, size_t length, ssize_t& nwrited, uint flags) const {
	ssize_t nwrite{ 0 };
	uint8_t* buffer{ (uint8_t*)data };
	nwrited = 0;
	while (length && (nwrite = SSL_write(ssl(), buffer, (int)length)) > 0) {
		length -= nwrite; nwrited += nwrite; buffer += nwrite;
	}
	switch (int err = SSL_get_error(ssl(), (int)nwrite); err) {
	case SSL_ERROR_NONE:
		return 0;
	case SSL_ERROR_WANT_READ:
	case SSL_ERROR_WANT_WRITE:
		return -EAGAIN;
		break;
	case SSL_ERROR_ZERO_RETURN:
		return -ECONNABORTED;
		break;
	default:
		break;
	}
	return -EBADFD;
}

ssize_t chttp::cfd::read_nossl(void* data, size_t length, ssize_t& nreaded, uint flags) const {
	ssize_t nread{ 0 };
	uint8_t* buffer{ (uint8_t*)data };
	nreaded = 0;
	while (length && (nread = ::recv((int)fdSocket, buffer, length, flags)) > 0) {
		length -= nread; nreaded += nread; buffer += nread;
	}
	return nread > 0 && !length ? 0 : -errno;
}

ssize_t chttp::cfd::read_ssl(void* data, size_t length, ssize_t& nreaded, uint flags) const {

	ssize_t nread{ 0 };
	uint8_t* buffer{ (uint8_t*)data };
	nreaded = 0;
	while (length && (nread = SSL_read(ssl(), buffer, (int)length)) > 0) {
		length -= nread; nreaded += nread; buffer += nread;
	}
	switch (int err = SSL_get_error(ssl(), (int)nread); err) {
	case SSL_ERROR_NONE:
		return 0;
	case SSL_ERROR_WANT_READ:
	case SSL_ERROR_WANT_WRITE:
		return -EAGAIN;
		break;
	case SSL_ERROR_ZERO_RETURN:
		return -ECONNABORTED;
		break;
	default:
		break;
	}
	return -EBADFD;
}