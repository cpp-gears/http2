#include "cwebsocket.h"
#include <cstring> 
#include <sys/epoll.h>
#include <vector>
#include <sys/socket.h>
#include <ctime>
#include <random>
#include <poll.h>
#include "chttpsha1.h"
#include "chttpbase64.h"
#include "chttp.h"

using namespace lib;

namespace lib::websocket {
#pragma pack(push,1)
	union extra_t
	{
		struct small {
		} small;
		struct medium {
			uint16_t len;
		} medium;
		struct large {
			uint64_t len;
		} large;
	};
	using mask_t = uint8_t[4];

	struct frame_t {
		uint8_t opcode : 4;
		uint8_t rsv3 : 1;
		uint8_t rsv2 : 1;
		uint8_t rsv1 : 1;
		uint8_t fin : 1;
		uint8_t len : 7;
		uint8_t mask : 1;
		frame_t() = default;
		frame_t(cwebsocket::opcode_t op, size_t length, bool have_mask = true, bool is_fin = true) :
			opcode{ (uint8_t)((int)op & 0xf) }, rsv3{ 0 }, rsv2{ 0 }, rsv1{ 0 }, fin{ is_fin }, len{ (uint8_t)(length & 0x7f) }, mask{ have_mask }
		{}
	};

	struct small_frame_t : frame_t {
		mask_t key; uint8_t payload[0];
		small_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,length,true,is_fin } { ; }
	};
	struct small_un_frame_t : frame_t {
		uint8_t payload[0];
		small_un_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,length,false,is_fin } { ; }
	};
	struct medium_frame_t : frame_t {
		uint16_t elen; mask_t key; uint8_t payload[0];
		medium_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,126,true,is_fin }, elen{ htobe16((uint16_t)length) } {; }
	};
	struct medium_un_frame_t : frame_t {
		uint16_t elen; uint8_t payload[0];
		medium_un_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,126,false,is_fin }, elen{ htobe16((uint16_t)length) } {; }
	};
	struct large_frame_t : frame_t {
		uint64_t elen; mask_t key; uint8_t payload[0];
		large_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,127,true,is_fin }, elen{ htobe64((uint64_t)length) } {; }
	};
	struct large_un_frame_t : frame_t {
		uint64_t elen; uint8_t payload[0];
		large_un_frame_t(cwebsocket::opcode_t op, size_t length, bool is_fin = true) : frame_t{ op,127,false,is_fin }, elen{ htobe64((uint64_t)length) } {; }
	};

#pragma pack(pop)
	static std::string SecKey(const std::string& secKey);
	static void Mask(uint8_t* data, size_t length, const uint8_t* mask);
	static inline void RandMask(mask_t& mask);
}

bool cwebsocket::wsIsConnectionUpgrade(const std::string_view& connection, const std::string_view& upgrade) {
	return (!connection.empty() && !upgrade.empty() && strncasecmp(connection.data(), "upgrade", 7) == 0 && strncasecmp(upgrade.data(), "websocket", 7) == 0);
}

ssize_t cwebsocket::wsReadResponse(http::fd_t fd, size_t max_response_size) {
	http::method_t method; http::headers_t headers; std::string_view code, msg; http::payload_t payload; std::shared_ptr<uint8_t[]> response;
	auto res = wsReadResponse(fd, method, code, msg, headers, payload, response, max_response_size);
	return res == 0 ? wsOnResponse(fd, method, code, msg, headers, payload, response) : wsOnError(fd, res);
}

ssize_t cwebsocket::wsReadResponse(http::fd_t fd, chttp::method_t& method, std::string_view& code, std::string_view& msg, chttp::headers_t& headers, chttp::payload_t& payload, std::shared_ptr<uint8_t[]>& response, size_t max_response_size) {
	return chttp::httpReadResponse(fd, method, code, msg, headers, payload, response, max_response_size);
}

ssize_t cwebsocket::wsReadRequest(http::fd_t fd, size_t max_request_size) {
	http::method_t method; http::uri_t uri; http::headers_t headers;
	http::payload_t payload;
	std::shared_ptr<uint8_t[]> request;
	auto res = wsReadRequest(fd, method, uri, headers, payload, request, max_request_size);
	return res == 0 ? wsOnRequest(fd, method, uri, headers, payload, request) : wsOnError(fd, res);
}

ssize_t cwebsocket::wsReadRequest(http::fd_t fd, chttp::method_t& method, chttp::uri_t& uri, chttp::headers_t& headers, chttp::payload_t& payload, std::shared_ptr<uint8_t[]>& request, size_t max_request_size) {
	return chttp::httpReadRequest(fd, method, uri, headers, payload, request, max_request_size);
}

ssize_t cwebsocket::wsWriteResponse(http::fd_t fd, const uint8_t* data, size_t length) {
	ssize_t nwrite{ 0 };
	auto res = fd.write(data, length, nwrite, MSG_NOSIGNAL);
	return res == 0 ? 0 : wsOnError(fd, res);
}

ssize_t cwebsocket::wsWriteMessage(http::fd_t fd, opcode_t opcode, const std::string_view& message, bool masked) {

	//
	std::vector<uint8_t> data;

	if (message.size() < 126) {
		if (!masked || message.size() == 0) {
			data.resize(sizeof(websocket::small_un_frame_t) + message.size());
			auto packet = new (data.data()) websocket::small_un_frame_t{ opcode,message.size() };
			std::memcpy(packet->payload, message.data(), message.size());
		}
		else {
			data.resize(sizeof(websocket::small_frame_t) + message.size());
			auto packet = new (data.data()) websocket::small_frame_t{ opcode,message.size() };
			websocket::RandMask(packet->key);
			std::memcpy(packet->payload, message.data(), message.size());
			websocket::Mask(packet->payload, message.size(), packet->key);
		}
	}
	else if (message.size() < 65536) {
		if (!masked) {
			data.resize(sizeof(websocket::medium_un_frame_t) + message.size());
			auto packet = new (data.data()) websocket::medium_un_frame_t{ opcode,message.size() };
			std::memcpy(packet->payload, message.data(), message.size());
		}
		else {
			data.resize(sizeof(websocket::medium_frame_t) + message.size());
			auto packet = new (data.data()) websocket::medium_frame_t{ opcode,message.size() };
			websocket::RandMask(packet->key);
			std::memcpy(packet->payload, message.data(), message.size());
			websocket::Mask(packet->payload, message.size(), packet->key);
		}
	}
	else {
		if (!masked) {
			data.resize(sizeof(websocket::large_un_frame_t) + message.size());
			auto packet = new (data.data()) websocket::large_un_frame_t{ opcode,message.size() };
			std::memcpy(packet->payload, message.data(), message.size());
		}
		else {
			data.resize(sizeof(websocket::large_frame_t) + message.size());
			auto packet = new (data.data()) websocket::large_frame_t{ opcode,message.size() };
			websocket::RandMask(packet->key);
			std::memcpy(packet->payload, message.data(), message.size());
			websocket::Mask(packet->payload, message.size(), packet->key);
		}
	}
	return wsWriteResponse(fd, data.data(), data.size());
}

ssize_t cwebsocket::wsReadMessage(http::fd_t fd, size_t max_request_size) {
	ssize_t nread{ 0 }, res{ 0 };
	websocket::frame_t packet;
	websocket::extra_t extra;
	websocket::mask_t mask;
	ssize_t dataLength{ 0 };

	if (res = fd.read(&packet, sizeof(packet), nread, MSG_NOSIGNAL); res == 0 && nread == sizeof(packet)) {
		if (auto length = packet.len; length < 126) {
			dataLength = length;
		}
		else if (length == 126) {
			if (res = fd.read(&extra, sizeof(websocket::extra_t::medium), nread, MSG_NOSIGNAL); res != 0 || nread != sizeof(websocket::extra_t::medium)) {
				return wsOnError(fd, nread > 0 ? -EMSGSIZE : res);
			}
			dataLength = be16toh(extra.medium.len);
		}
		else if (length == 127) {
			if (res = fd.read(&extra, sizeof(websocket::extra_t::large), nread, MSG_NOSIGNAL); res !=0 || nread != sizeof(websocket::extra_t::large)) {
				return wsOnError(fd, nread > 0 ? -EMSGSIZE : res);
			}
			dataLength = be64toh(extra.large.len);
		}
		else {
			return wsOnError(fd, -EBADMSG);
		}

		if (packet.mask) {
			if (res = fd.read(&mask, sizeof(websocket::mask_t), nread, MSG_NOSIGNAL); res !=0 || nread != sizeof(websocket::mask_t)) {
				return wsOnError(fd, nread > 0 ? -EMSGSIZE : res);
			}
		}

		if (dataLength < (ssize_t)max_request_size) {
			ssize_t nreaded{ 0 };
			http::payload_t reqData{ {},0 };
			if (dataLength) {
				reqData = http::payload_t{ new uint8_t[dataLength], dataLength };
				pollfd fds{ .fd = (int)fd.fd(),.events = POLLIN,.revents = 0 };
				do {
					if (res = fd.read(reqData.first.get() + nreaded, dataLength, nread, MSG_NOSIGNAL); nread > 0) {
						dataLength -= nread; nreaded += nread;
					}
					fds.revents = 0;
				} while (res == -EAGAIN && (poll(&fds, 1, 100) > 0));
			}

			if (res == 0) {
				if (nreaded == (ssize_t)reqData.second) {
					if (packet.mask) { websocket::Mask(reqData.first.get(), reqData.second, mask); }
					return wsOnMessage(fd, (opcode_t)packet.opcode, reqData);
				}
				res = -EMSGSIZE;
			}
		}
		else {
			res = -EMSGSIZE;
		}
	}
	else if (nread == 0) {
		return wsOnError(fd, -ECONNRESET);
	}
	return wsOnError(fd, res);
}

const std::string_view& cwebsocket::wsMethod(opcode_t code) {
	static const std::string_view methodNames[]{ "TEXT","CLOSE","BINARY","PING","PONG","MORE","INVALID" };
	switch (code)
	{
	case lib::cwebsocket::opcode_t::text:
		return methodNames[0];
	case lib::cwebsocket::opcode_t::close:
		return methodNames[1];
	case lib::cwebsocket::opcode_t::binary:
		return methodNames[2];
	case lib::cwebsocket::opcode_t::ping:
		return methodNames[3];
	case lib::cwebsocket::opcode_t::pong:
		return methodNames[4];
	case lib::cwebsocket::opcode_t::more:
		return methodNames[5];
	default:
		break;
	}
	return methodNames[6];
}

std::string cwebsocket::wsGenerateSecKey(){
	std::array<uint8_t,16> seckey;
	std::mt19937_64 rng(time(nullptr));
	std::uniform_int_distribution<uint8_t> unii(0, 255);
	
	for (size_t length{ 16 }; length--;) {
		seckey[length] = unii(rng);
	}
	return libhttp::cbase64::encode(seckey.data(), seckey.size());
}

http::payload_t cwebsocket::wsMakeRequest(const std::string& uri, const std::string& host, const std::string& secKey, const std::string secSubProtocol, const std::string secOrigin, const std::string secVersion) {
	std::string buffer;
	buffer.assign("GET ").append(uri).append(" HTTP/1.1\r\n");
	buffer.append("Host: ").append(host).append("\r\n");
	buffer.append("Upgrade: websocket\r\nConnection: Upgrade\r\n");
	buffer.append("Sec-WebSocket-Key: ").append(secKey).append("\r\n");
	buffer.append("Sec-WebSocket-Version: ").append(secVersion).append("\r\n");
	if (!secOrigin.empty()) { buffer.append("Origin: ").append(secOrigin).append("\r\n"); }
	if (!secSubProtocol.empty()) { buffer.append("Sec-WebSocket-Protocol: ").append(secSubProtocol).append("\r\n"); }
	buffer.append("\r\n");

	http::payload_t request{ new uint8_t[buffer.length()],buffer.length() };

	if (request.first) {
		std::memcpy(request.first.get(), buffer.data(), buffer.length());
		return request;
	}
	return {};
}

http::payload_t cwebsocket::wsMakeResponse(const std::string& url, const std::string& secKey, const std::string& secSubProtocol, const std::string& secOrigin, const std::string& secVersion) {
	std::string buffer;
	buffer.assign(
		"HTTP/1.1 101 Switching Protocols\r\n"\
		"Upgrade: WebSocket\r\n"\
		"Connection: Upgrade\r\n"
	);
	buffer.append("Sec-WebSocket-Accept: ").append(websocket::SecKey(secKey)).append("\r\n");
	buffer.append("Sec-WebSocket-Version: ").append(!secVersion.empty() ? secVersion : "13").append("\r\n");
	if (!secSubProtocol.empty()) { buffer.append("Sec-WebSocket-Protocol: ").append(secSubProtocol).append("\r\n"); }

	buffer.append("\r\n");

	http::payload_t request{ new uint8_t[buffer.length()],buffer.length() };

	if (request.first) {
		std::memcpy(request.first.get(), buffer.data(), buffer.length());
		return request;
	}
	return {};
}

inline void websocket::RandMask(mask_t& mask) {
	srand48(std::time(nullptr));
	*(uint32_t*)mask = (uint32_t)lrand48();
}

void websocket::Mask(uint8_t* data, size_t length, const uint8_t* mask) {
	for (size_t n{ 0 }; n < length; ++n) { data[n] ^= mask[n % 4]; }
}

std::string websocket::SecKey(const std::string& secKey) {
	libhttp::csha1::buffer_t result;
	libhttp::csha1 digest;
	digest.add(secKey.data(), secKey.size());
	digest.add("258EAFA5-E914-47DA-95CA-C5AB0DC85B11", sizeof("258EAFA5-E914-47DA-95CA-C5AB0DC85B11") - 1);
	digest.get(result);
	return libhttp::cbase64::encode(result.data(), result.size());
}
