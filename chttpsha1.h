#pragma once
#include <cstdint>
#include <cstdlib>
#include <array>

namespace libhttp {
	class csha1 {
	public:
		static inline constexpr size_t  size = 20;
		using buffer_t = std::array<uint8_t, size>;

		csha1();

		/// compute SHA1 of a memory block
		csha1& operator()(const void* data, size_t numBytes);

		/// add arbitrary number of bytes
		void add(const void* data, size_t numBytes);

		/// return latest hash as bytes
		void get(buffer_t& buffer);

		/**
			* @brief get has as hex string
			* @return hex string
		*/
		std::string to_string();

		/// restart
		void reset();

	private:
		static inline constexpr size_t  BlockSize = 512 / 8, HashValues = size / 4;

		/// process 64 bytes
		void processBlock(const void* data);
		/// process everything left in the internal buffer
		void processBuffer();

		/// size of processed data in bytes
		uint64_t m_numBytes;
		/// valid bytes in m_buffer
		size_t   m_bufferSize;
		/// bytes not processed yet
		uint8_t  m_buffer[BlockSize];

		/// hash, stored as integers
		uint32_t m_hash[HashValues];
	};
}
