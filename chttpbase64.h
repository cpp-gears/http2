#pragma once
#include <string>
#include <vector>

namespace libhttp {
	class cbase64 {
	public:
		static std::string encode(const uint8_t* source, std::size_t length);
		static std::vector<uint8_t> decode(const uint8_t* source, std::size_t length);

		static std::string encodesafe(const uint8_t* source, std::size_t length);
		static std::vector<uint8_t> decodesafe(const uint8_t* source, std::size_t length);
	};
}