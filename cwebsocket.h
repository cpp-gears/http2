#pragma once
#include "chttp.h"

extern "C" {
#include <openssl/ssl.h>
#include <openssl/err.h>
}

namespace lib {
	class cwebsocket {
	public:
		enum class opcode_t : uint8_t {
			more = 0x0, text = 0x1, binary = 0x2, close = 0x8, ping = 0x9, pong = 0xa
		};

		static http::payload_t wsMakeRequest(const std::string& uri, const std::string& host, const std::string& secKey, const std::string secSubProtocol = {}, const std::string secOrigin = {}, const std::string secVersion = { "13" });
		static http::payload_t wsMakeResponse(const std::string& url, const std::string& secKey, const std::string& secSubProtocol = {}, const std::string& secOrigin = {}, const std::string& secVersion = { "13" });

		static std::string wsGenerateSecKey();

		static const std::string_view& wsMethod(opcode_t);

	protected:

		bool wsIsConnectionUpgrade(const std::string_view& connection, const std::string_view& upgrade);

		ssize_t wsReadRequest(http::fd_t fd, size_t max_request_size);
		ssize_t wsReadRequest(http::fd_t fd, http::method_t& method, http::uri_t& uri, http::headers_t& headers, http::payload_t& payload, std::shared_ptr<uint8_t[]>& request, size_t max_request_size);

		ssize_t wsReadResponse(http::fd_t fd, size_t max_response_size);
		ssize_t wsReadResponse(http::fd_t fd, http::method_t& method, std::string_view& code, std::string_view& msg, http::headers_t& headers, http::payload_t& payload, std::shared_ptr<uint8_t[]>& response, size_t max_response_size);

		ssize_t wsWriteResponse(http::fd_t fd, const uint8_t* data,size_t length);

		ssize_t wsReadMessage(http::fd_t fd, size_t max_request_size);
		ssize_t wsWriteMessage(http::fd_t fd, opcode_t opcode, const std::string_view& message, bool masked = false);

		virtual inline ssize_t wsOnError(http::fd_t fd, ssize_t err) { return err; }
		virtual inline ssize_t wsOnRequest(http::fd_t fd, const http::method_t& type, const http::uri_t& uri, const http::headers_t& headers, const http::payload_t& payload, std::shared_ptr<uint8_t[]>& request) { return 0; }
		virtual inline ssize_t wsOnResponse(http::fd_t fd, const http::method_t& type, const std::string_view& code, const std::string_view& msg, const http::headers_t& headers, const http::payload_t& payload, std::shared_ptr<uint8_t[]>& response) { return 0; }
		virtual inline ssize_t wsOnMessage(http::fd_t fd, opcode_t opcode, const http::payload_t& message) { return 0; }
	};
}