#pragma once
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <regex>

extern "C" {
#include <openssl/ssl.h>
#include <openssl/err.h>
}

namespace lib {
	class chttp
	{
	public:
		class cfd {
		private:
			ssize_t write_nossl(const void* data, size_t length, ssize_t& nwrite, uint flags) const;
			ssize_t write_ssl(const void* data, size_t length, ssize_t& nwrite, uint flags) const;
			ssize_t read_nossl(void* data, size_t length, ssize_t& nread, uint flags) const;
			ssize_t read_ssl(void* data, size_t length, ssize_t& nread, uint flags) const;
		public:
			cfd(ssize_t so, std::shared_ptr<SSL> ssl) : fdSocket{ so }, fdSsl{ ssl }{ if (ssl) { write_fn = &cfd::write_ssl; read_fn = &cfd::read_ssl;  } }
			cfd(const cfd& fd) : fdSocket{ fd.fdSocket }, fdSsl{ fd.fdSsl }, write_fn{ fd.write_fn }, read_fn{ fd.read_fn }{; }
			inline ssize_t write(const void* data, size_t length, ssize_t& nwrite, uint flags) const { return (this->*write_fn)(data, length, nwrite, flags); }
			inline ssize_t read(void* data, size_t length, ssize_t& nread, uint flags) const { return (this->*read_fn)(data, length, nread, flags); }
			inline int fd() const { return (int)fdSocket; }
			inline SSL* ssl() const { return fdSsl.get(); }
		public:
			ssize_t fdSocket;
			std::shared_ptr<SSL> fdSsl;
		private:
			ssize_t(cfd::* write_fn)(const void* data, size_t length, ssize_t& nwrite, uint flags) const { &cfd::write_nossl };
			ssize_t(cfd::* read_fn)(void* data, size_t length, ssize_t& nread, uint flags) const { &cfd::read_nossl };
		};
	public:
		using fd_t = const cfd&;
	private:
#ifndef HTTP_MAX_HEADER_SIZE
		static inline constexpr size_t MaxHeaderSize{ 8192 };
#else
		static inline constexpr size_t MaxHeaderSize{ HTTP_MAX_HEADER_SIZE };
#endif // !HTTP_MAX_HEADER_SIZE

		class cheaders : protected std::unordered_map<std::string_view, std::string_view> {
			using container_t = std::unordered_map<std::string_view, std::string_view>;
		public:
			inline std::string_view at(const std::string_view& header, std::string_view def = {}) const { if (auto&& it{ container_t::find(header) }; it != container_t::end()) { return it->second; } return def; }
			inline std::string str(const std::string_view& header) const { return std::string{ at(header) }; }
			size_t number(const std::string_view& header, int base = 10) const { if (auto&& val{ str(header) }; !val.empty()) { try { return std::stoull(val, 0, base); } catch (...) {} } return 0; }
			std::unordered_map<std::string_view, std::string_view> params(const std::string_view& header) const;
			std::unordered_set<std::string_view> explode(const std::string_view& header, const char separator = ',') const;
			bool match(const std::string_view& header, const std::regex& re, std::vector<std::string_view>&) const;
			inline auto emplace(const std::string_view& k, const std::string_view& v) { return  container_t::emplace(k, v); }
			inline auto begin() const { return container_t::begin(); }
			inline auto end() const { return container_t::end(); }
			inline auto size() const { return container_t::size(); }
			inline void clear() { container_t::clear(); }
			inline void operator = (const cheaders& hs) { container_t::insert(hs.begin(), hs.end()); }
			inline bool is(const std::string_view& header) const { return container_t::find(header) != container_t::end(); }
		};
		class curi {
			class cargs : protected std::unordered_multimap<std::string_view, std::string_view> {
				using container_t = std::unordered_multimap<std::string_view, std::string_view>;
			public:
				inline std::string_view at(const std::string_view& header, std::string_view def = {}) const { if (auto&& it{ container_t::find(header) }; it != container_t::end()) { return it->second; } return def; }
				inline std::string str(const std::string_view& header) const { return std::string{ at(header) }; }
				size_t number(const std::string_view& header, int base = 10) const { if (auto&& val{ str(header) }; !val.empty()) { try { return std::stoull(val, 0, base); } catch (...) {} } return 0; }
				inline auto emplace(const std::string_view& k, const std::string_view& v) { return container_t::emplace(k, v); }
				inline auto begin() const { return container_t::begin(); }
				inline auto end() const { return container_t::end(); }
				inline auto size() const { return container_t::size(); }

				inline void operator = (const cargs& args) { container_t::insert(args.begin(), args.end()); }
			};
			class cpath : protected std::deque<std::string_view> {
			public:
				inline std::string_view at(size_t idx) const { if (idx < std::deque<std::string_view>::size()) { return std::deque<std::string_view>::operator[](idx); } return {}; }
				inline std::string str(size_t idx) const { return std::string{ at(idx) }; }
				size_t number(size_t idx, int base = 10) const { if (auto&& val{ str(idx) }; !val.empty()) { try { return std::stoull(val, 0, base); } catch (...) {} } return 0; }
				bool match(size_t arg, const std::regex& re, std::vector<std::string_view>&) const;
				bool match(size_t arg, const std::regex& re, std::string_view&) const;
				bool match(size_t arg, const std::string_view& needle) const;
				inline auto emplace(const std::string_view& k) { return std::deque<std::string_view>::push_back(k); }
				inline auto begin() const { return std::deque<std::string_view>::begin(); }
				inline auto end() const { return std::deque<std::string_view>::end(); }
				inline auto size() const { return std::deque<std::string_view>::size(); }
				inline void operator = (const cpath& args) { assign(args.begin(), args.end()); }
			};
		public:
			inline void operator = (const curi& uri) { 
				full = uri.full; schema = uri.schema; hostport = uri.hostport; host = uri.host; port = uri.port; url = uri.url; path = uri.path; args = uri.args; pathlist = uri.pathlist; argslist = uri.argslist; 
			}
			void assign(std::string_view uri);
			std::string_view full, schema;
			std::string_view hostport, host, port, url, path, args;
			cpath pathlist;
			cargs argslist;
		};
	public:
		using headers_t = cheaders;
		using uri_t = curi;
		using method_t = std::string_view;
		using payload_t = std::pair<std::shared_ptr<uint8_t[]>, size_t>;
	public:
		chttp();
		virtual ~chttp();

		static chttp::payload_t httpMakeResponse(ssize_t httpCode, std::string_view httpMsg, const std::unordered_map<std::string, std::string>& httpHeaders, const void* payloadData = nullptr, size_t payloadLen = 0);
		//static chttp::payload_t httpMakeRequest(std::string, std::string_view httpMsg, const std::unordered_map<std::string, std::string>& httpHeaders, const void* payloadData = nullptr, size_t payloadLen = 0) {

		ssize_t httpReadRequest(chttp::fd_t fd, size_t max_request_size);
		ssize_t httpReadResponse(chttp::fd_t fd, size_t max_response_size);

		static ssize_t httpReadRequest(chttp::fd_t fd, chttp::method_t& method, chttp::uri_t& uri, chttp::headers_t& headers, chttp::payload_t& payload, std::shared_ptr<uint8_t[]>& request, size_t max_request_size);
		static ssize_t httpReadResponse(chttp::fd_t fd, chttp::method_t& method, std::string_view& code, std::string_view& msg, chttp::headers_t& headers, chttp::payload_t& payload, std::shared_ptr<uint8_t[]>& response, size_t max_response_size);

		static std::string_view httpCodeMessage(ssize_t code, std::string_view msg = {});
	protected:
		virtual ssize_t httpOnError(chttp::fd_t fd, ssize_t err) { return err; }
		virtual ssize_t httpOnRequest(chttp::fd_t fd, const chttp::method_t& method, const chttp::uri_t& uri, const chttp::headers_t& headers, const chttp::payload_t& payload, std::shared_ptr<uint8_t[]> request) { return 0; }
		virtual ssize_t httpOnResponse(chttp::fd_t fd, const chttp::method_t& method, const std::string_view& code, const std::string_view& msg, const chttp::headers_t& headers, const chttp::payload_t& payload, std::shared_ptr<uint8_t[]> response) { return 0; }
	};
}

using http = lib::chttp;
